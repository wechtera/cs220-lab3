/*  file name : calc.l  */

/*  lexer for arithmetic expression calculator.  */
/*  See bison specification  calc.y             */

%{
#define YYSTYPE double
#include "calc.tab.h"
%}

DIGIT  [0-9]

%%

{DIGIT}+("."{DIGIT}+)?  {
          yylval = atof(yytext); return NUMBER; }

[ \t]     /* ignore whitespace */

<<EOF>>   yyterminate();  /* signal end of dialogue */

\n        return yytext[0];
.         return yytext[0];

%%
