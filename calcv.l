/*  file name : calcv.l  */

/*  lexer for calculator grammar with variables and real values. */
/*  See bison specification  calcv.y                             */
/*  derived from "lex and yacc" ch3-03.l  (page 65)              */

%{
#include "calcv.tab.h"
%}

DIGIT  [0-9]

%%

{DIGIT}+("."{DIGIT}+)?  {
          yylval.dval = atof(yytext); return NUMBER; }

[ \t]     /* ignore whitespace */

[a-z]     yylval.vblno = yytext[0] - 'a';  return NAME; 

<<EOF>>   yyterminate();  /* signal end of dialogue */

\n       return yytext[0];
.        return yytext[0];

%%
