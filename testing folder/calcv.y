/*  file name : calcv.y  */

/*  Calculator grammar with variables and float values. */
/*  Variables are single letters and symbol table is a  */
/*  simple array of double, indexed by variable names.  */
/*  Associativity and precedence declarations used to   */
/*  disambiguate the grammar.                           */
/*  Derived from "lex and yacc" ch3-03.y  (page 64)     */

%{

#include <stdio.h>

double vbltable[26];

%}

%union {
    double dval;
    int vblno;
}

%token  <vblno> NAME
%token  <dval>  NUMBER
%token  <dval>  EQUAL/*variable for seeing if two numbers are equal*/

%type <dval> expression
%type <dval> term
%type <dval> factor

%%

input
    :  /* empty */
    |  input line
    ;

line
    : '\n'
    |  NAME '=' expression  '\n'  { vbltable[$1] = $3; }
    |  expression '\n'            {  printf ("\t= %.2f\n", $1); }
    ;

expression
    : expression '+' term   { $$ = $1 + $3; }
    | expression '-' term   { $$ = $1 - $3; }
    | expression '^' term   {$$ = $1 + $3;}
    | expression EQUAL term {if($2 == $3)/*our implementation of seeing if two numbers are equal*/
				
				{
				printf("Matching, the number matched is: ");/*numbers match*/
				$$=$2;}
				else
				{
				printf("Not Matching, second unmatched number is: ");/*numbers do not match*/
				$$=$3;};}
    | term                  { $$ = $1; }
    ;
term
    : term '*' factor       { $$ = $1 * $3; }
    | term '/' factor       { if ($3 == 0.0)
                                 yyerror("divide by zero");
                              else
                                 $$ = $1 / $3; }
    | factor                { $$ = $1; }
    ;
factor
    : '-' factor            { $$ = - $2; }
    | '(' expression ')'    { $$ = $2; }
    |  NUMBER               { $$ = $1; } 
    |  NAME                 { $$ = vbltable[$1]; }
    ;

%%


main ()
{
  yyparse ();
}

yyerror (char *s)  /* Called by yyparse on error */
{
  printf ("\terror: %s\n", s);
}
